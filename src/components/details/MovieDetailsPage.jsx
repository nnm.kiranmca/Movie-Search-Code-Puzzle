import { useSelector } from "react-redux";
import { useParams } from 'react-router-dom';
import { DataGrid, GridColDef } from "@mui/x-data-grid";

// Grid referenced here for usage of a details page, but feel free to
// use whatever you like for this.;
const MovieDetailsPage = (props) => {
  // Use hooks to retrieve the details using the imdbID, passed as the parameter id
  const { id } = useParams();
console.log(id);
let moves = useSelector(state => state.appReducer.moves);
const details = moves.filter(det => det.imdbID == id);

const columns: GridColDef<(typeof rows)[number]>[] = [
  { 
    field: 'imdbID', 
    headerName: 'ID', 
    width: 100,
  },
  {
    field: 'Title',
    headerName: 'Title',
    width: 200,
    editable: true,
  },
  {
    field: 'Year',
    headerName: 'Year',
    width: 100,
    editable: true,
  },
  {
    field: 'Rated',
    headerName: 'Rated',
    width: 100,
    editable: true,
  },
  {
    field: 'Runtime',
    headerName: 'Runtime',
    width: 100,
    editable: true,
  },
  {
    field: 'Released',
    headerName: 'Released',
    width: 100,
    editable: true,
  },
  {
    field: 'Poster',
    headerName: 'Poster',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 300,
    height: 500,
    renderCell: (params) => <img src={params.value} />
  },
];

  // render details view
  return (
    <div>
    <div>
      <h3>Movie Details</h3>
    </div>
    <div class="container" style={{ height: 500, width: '100%',verticalAlign: 'middle', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      {moves.filter(det => det.imdbID == id).map(filteredMovies => (
          <div style={{ height: 500, width: '70%' }}>
          <DataGrid
            rows={details?details:[]}
            columns={columns}
            rowHeight={300}
            getRowId={(row) =>  row.imdbID}
            enablePagination={false}
          />
        </div>
      ))}
    </div>
    </div>
  )
};

export default MovieDetailsPage;
