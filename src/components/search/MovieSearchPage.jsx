import React, { useState } from "react";
import { useDispatch } from "react-redux";
import * as types from "../../redux/actions/actionTypes";
// Data grid to show list of movie search results, but use whatever you like here.
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import getMovieRequest from '../../redux/actions/movieActions'
import { useNavigate } from 'react-router-dom';


// Text field to allow user to enter search text, but use whatever you like here. 


const MovieSearchPage = (props) => {  
  const [isLoading, setIsLoading] = useState(false);
  const history = useNavigate();
  const [movieData, setmovieData] = useState(null);
  const dispatch = useDispatch();
  // Use hooks to retrieve list of movies based on user input of search text.  After user enters 4th character in search box,
  // retrieval should happen automatically.
  const columns: GridColDef<(typeof rows)[number]>[] = [
    { 
      field: 'imdbID', 
      headerName: 'ID', 
      width: 200,
    },
    {
      field: 'Title',
      headerName: 'Title',
      width: 450,
      editable: true,
    },
    {
      field: 'Year',
      headerName: 'Year',
      width: 250,
      editable: true,
    },
    {
      field: 'Poster',
      headerName: 'Poster',
      sortable: false,
      width: 100,
      renderCell: (params) => <img src={params.value} />
    },
  ];
  const handleRowClick = (event) => {
    history(`/details/${event.id}`);    
  };
  
  const UpdateOtherDetails = (movies) => {
    const movieList = movies.map(m => ({
      ...m,
      Rated:"R",
      Released: "24 Aug 2007",
      Runtime:"103 min"
    }));
    return movieList;
  };
  // Render search results here.  Refer to README for specifications.
  function handleChange(searchValue) {
    
    if(searchValue.length > 2)
    {
      setIsLoading(true);
      console.log(searchValue);
      getMovieRequest(searchValue).then(res=> {
        setmovieData(res);
        if(res != null){
          dispatch(types.addToCache(UpdateOtherDetails(res)));
        }  
        setIsLoading(false);    
      });
    }
  }
  return (
    <>
      <div className='container-fluid movie-app'>
      
     <div className="row">
     <h3></h3>
      </div> 
			<div class="container" style={{ paddingTop:30, height: 600, width: '100%',verticalAlign: 'middle', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      
			<div style={{ width: '70%' }}>
      <div className="row">
        <table>
          <tr>
            <td>
              <h3>Search Movie Name</h3>
            </td>
            <td>
                <input
                style={{height: 20, width: 300}}
                  className='form-control'	
                  onChange={(event) => handleChange(event.target.value)}
                  placeholder='search...'	/>
            </td>
          </tr>
        </table>          
      </div>
      <div className="row" style={{minHeight: 500}}>
      {
        isLoading ? <p>Loading...</p> : 
        <DataGrid style={{minHeight: 200}}
          rows={movieData?movieData:[]}
          columns={columns}
          getRowId={(row) =>  row.imdbID}
          onRowClick={handleRowClick}                  
        />
      }
      </div>
      
    </div>
		
			</div>
		</div>
    </>
  )
};

export default MovieSearchPage;
