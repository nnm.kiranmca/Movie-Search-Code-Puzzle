import { combineReducers } from "redux";
import appReducer from "./movieReducer";

const rootReducer = combineReducers({
  appReducer,
});

export default rootReducer;
