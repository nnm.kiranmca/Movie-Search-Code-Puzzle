import * as types from "../actions/actionTypes";
import { getRequest } from "../../shared/httpRequest";
import { useSelector } from 'react-redux';

const initialState = {
  // Set up initial state for movies store as needed. 
  moves:[] 
  
};

// Define movieReducer function here.
const appReducer = (state = initialState, action) => {
  // The reducer normally looks at the action type field to decide what happens
  switch (action?.type) {
    case "ADD":
          return {
            ...state,
            moves: action.payload,
          };
    // Do something here based on the different types of actions
    default:
      // If this reducer doesn't recognize the action type, or doesn't
      // care about this specific action, return the existing state unchanged
      return {
        ...state
      };
  }
}
export default appReducer;