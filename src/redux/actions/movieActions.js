import { getRequest } from "../../shared/httpRequest";

// Add movie action functions here as needed.  Use the getRequest function imported above to retrieve
const getMovieRequest = async (search) => {
    const url = `http://www.omdbapi.com/?s=${search}&plot=full&apikey=263d22d8`;    
    
    const response = await getRequest(url);
    console.log(url);
    const responseJson = await response.data;

    if (responseJson.Search) {
        
        return responseJson.Search;
    }
};
export default getMovieRequest;


