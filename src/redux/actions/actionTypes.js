// Add action types here as needed.
export const addToCache = (data) => ({
    type: "ADD",
    payload: data,
  });
